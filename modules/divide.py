from functools import reduce
def divide(*args):
    return reduce(lambda a, b: a / b, args)